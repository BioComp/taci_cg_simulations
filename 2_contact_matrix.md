[ Previous: Simulations ](./1_simulations.md)

# Table of content <!-- omit in toc -->

[[_TOC_]] <!-- omit in toc -->

Here, you will find instructions to calculate a contact maps between residues.

# Requirements
- GROMACS, version 2016 or superior
- R version 3+
- Suggested: 
    * RStudio Version 1.4
    * Libraries ggplot2 and reshape, for graphs in R
  

# Procedure

Here we describe the procedure to calculate a matrix of contacts between peptides at the residue level. The contacts are calculated based on the distance between BB particles of different peptides along the simulation. 

In detail, for each residue ($`i`$) of every peptide ($`{H}`$) it is computed along the simulation time ($`T`$) the number of contacts against all the other residues in the remaining helices. A contact was defined when the BB atoms of two residues are located at $XYZ$-distance equal to or less than an arbitrary cut-off, as follows:


```math
 C_{ij}^{HK} =
\begin{cases}
  1, & \mathrm{if} || \mathbf{r}_i^H - \mathbf{r}_j^K || \le d_{\mathrm{cutoff}}  \\
  0, & \mathrm{if} || \mathbf{r}_i^H - \mathbf{r}_j^K || > d_{\mathrm{cutoff}}  
\end{cases}
```

where $`i`$ and $`j`$ are the residue number in the peptide sequence ($`i=\{1,\ldots,j,\ldots, N\}`$), $`\mathrm{d_c}`$ is a cutoff distance, and $`H`$ and $`K`$ are the helices analyzed ($`H=\{1,\ldots,K,\ldots,36\}`$) . Thus, the number of contacts (NC) for every residue $`i`$ against each residue $`j`$ in the remaining ($`K`$) helices were computed as.

```math
NC_{ij} = \sum_T \sum_{j}^{K\ne{H}} C_{ij}^{HK}
```

# Requirements
- GNU bash, versión > 5
- GROMACS,  version > 2016
- R version > 3
- Suggested: 
  - RStudio Version 1.4
  - For graphs in R, libraries ggplot2 and reshape

>**Note:** If your simulations did not finished but you want to try this method, you can use the files in the `example_files` folder. The scripts have indictations to modify the commands in order to use these files. These files are:

>- `traj_protein.xtc`. A compressed molecular dynamics file of 6 microseconds. It  contains only the coordinates of the 9 peptides, **and not** the lipids, salts or water. It is intended to be lightweight
>- `conf_for_topol.gro`. A file with the initial configuration of the peptides (not lipids, waters or salts), to be used as topology file (in replacement of `topol.tpr`, that contains information of the whole system and is very dependent on the GROMACS version, so it would be useless with the `.xtc` file provided with in this example).


In this procedure, the contact detection along the simulation is performed by a GROMACS tool. You have to provide this tool with the number of the BB particle `i` and the number of all other BB particles of the rest of the peptides to check for possible contacts. This procedure is repeated for every BB particle. The number of particle `i` and particles `j` are listed in two groups in an index file (the syntax is specific of GROMACS). In the procedure, you have to create one index file per particle `i`. In our example, every peptide has 36 BB particles. Thus, for a system with 9 peptides you have to create 36 x 9 - 1 =  323 index files. GROMACS has a tool to create this type of files, but we use a Rscript to automate this step. Then we used the GROMACS tool `select` to compute the contacts. Finally we process the output of the previous step with another Rscript to calculate the contact matrix. The matrix has dimension NxN , where N is the number of residues.


## Indices files

In your computer, move to the first working directory (`PROT`, in this example) and copy the folder `contact_map` in this repository and move there. If you are in the folder `PROT`, use these command:
`cp -r ../contact_maps/ ./`
`cd ./contact_maps/`

The next steps generate a great number of temporary files, so it is better to create two new directories, one for the index files (`/indices`) and another for the output of the `select` command (`/output_contacts`).
`mkdir ./indices`
`mkdir ./ouput_contacts`

Create the index files. First, copy the atom number of the BB particles from one (the first) peptide to a file named `one_peptide_BB_atoms` and atom number of the BB particles from all peptides to a file named `all_peptides_BB_atoms`. We provide the next bash command to extract these numbers from two `.gro` files, which in our example are one step before in the directory tree. 

```
# If you followd this instructions ../ is the correct directory.
# Change in necessary

grep BB ../z_aligned_peptide.gro  | awk '{print $3}' > ./indices/one_peptide_BB_atoms
grep BB ../peptide_array_mem.gro  | awk '{print $3}' > ./indices/all_peptides_BB_atoms
```

>**Note:** Check the output to be sure the numbers are correct and no other line was copied.

Create a series of `.ndx` GROMACS index files with a Rscript `indices.R` that you will find in the current directory. Open `indices.R` (you can use a text editor or RStudio) and edit the variables indicated in the Rscript as you need. Then, you can run the Rscript in RStudio or directly in the terminal with the next command.

`Rscript ./indices.R`

## Contacts

Next, using the GROMACS command `select`, we compute the contacts between the BB particle in `[ group1 ]` and all other BB particles in `[ group2 ]`, as defined in every `.ndx` file generated in the previous step (open any of the index files to verify the syntax). The output files of this step are saved in `./ouput_contacts`. The GROMACS `select` command is executed with a bash script.

Using a text editor, open the script `contacts.sh`  in the current directory and edit the variables as you need: cutoff to filter interactions by distance, name of directories, etc. 

>**Note:** You can check `gmx select -h`  for better understand the options we selected and others you can choose (ie. initial and final time of analysis). For example, we used `-pbc` to detect contacts through periodic boundaries and `-dt 100` to control the time step of our analysis.

>**Note:** follow the instructions in the `contacts.sh` script to use the files in the `example_files` directory 

After editing your options, grant execution permission and run the script.

```
# Concede execution permission for the script
chmod +x ./contacts.sh

#Run the script
./contacts.sh

```


## Contact matrix

Now, it is time to use `matrix.R` to calculate how many times $`BB_{i}`$ of any helix contacted $`BB_{j}`$ of the other remaining helices. 

Open this script and edit your options as indicated (directories, number of atoms per peptide). Run the script in RStudio or in the terminal with the next command. The script will show you the number of BB particles per peptide and in the whole system, and will output the matrix in the shell. 

```
Rscript matrix.R
```

If the script runs successfully two files are created:  `ContactMat.RDS` and  `ContactMat.csv`. These files contain the same matrix of raw number of contacts in two different formats. 

Also two graphs are produce. `Contact_map.png` will be created with the normalized contact map in the format we used in our work.

![](./Figs/Contact_map.png)

In some cases it is useful to exclude the residues in the aqueous phase from the matrix graph. The script also produce the plot in `Contact_map_buried_residues.png` considering only the residues buried in the membrane.

![](./Figs/Contact_map_buried_residues.png)

[Next: Radial and angular distribution](./3_radial_distribution.md)