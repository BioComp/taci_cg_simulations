[Previous: Contact analysis ](./2_contact_matrix.md)

# Table of content<!-- omit in toc -->

[[_TOC_]]

Here, you will find instructions to calculate a radial distribution around every peptide in the simulation.

## Requirements

- GROMACS, version 2016 or superior
- R version 3+
- Suggested: 
    * RStudio Version 1.4
    * For graphs in R, libraries ggplot2 and reshape

## Procedure

***NOTE:** If your simulations did not finished but you want to try this method, you can download the files in the `example_files` folder. The scripts have indictations to modify the commands in order to use these files.*


Before you begin. In this protocol we are going to analyze the structural organization of the contacts between peptides. We are going to build maps of radial distribution and angle orientation between peptides in the membrane $XY$-plane. In this analysis, first we calculate a centroid per peptide at the level of a selected residue. Then every centroid  in turn is considered a center of coordinates, and the occupancy and orientation of the surrounding peptides along the simulation time range is computed.


To compute a centroid ($`\mathbf{C}`$), we define a central residue and then take the coordinates of its BB particle ($`i`$) and of the BB particles of the previous and next residue in the sequence ($`\mathbf{C}_i =(\mathbf{r}_{i-1}+\mathbf{r}_{i}+\mathbf{r}_{i+1}/3`$, where $`\mathbf{r} `$ is the $`XYZ `$-coordinate of the atom). Then, the unit bisection vector is computed between the central ($`i `$) and adjacent BB particles ($`i\pm 1 `$). Finally, the centroid is considered the origin of the reference frame with its orientation vector aligned in the $`X `$-axis and the position and orientation of the centroids of the remaining peptides (8 in this example) are computed. This procedure is repeated for every peptide along a certain simulation time. The scatter plot of the accumulated $`XY `$-centroids positions and orientations are transformed to a density map with `ggplot` implemented in `R`.

!["Scheme"](./Figs/scheme.png)

In our analysis we are going to compute the next variables, which are illustrated in the diagram:


- (*vdx, vdy*), the location of the centroid of the peptide h1 with respect to the centroid of the reference peptide (href).
- *Alpha angle*. The angle corresponding to the location of the centroid of the peptide h1 with respect to the centroid of the reference peptide (href).
- *Beta angle*. The angle between the orientation angle of the peptide h1 with respect to the orientation angle  of the reference peptide (href).

Return to the first directory (`PROT` in this example), copy the folder `radial_distribution` and move there. If you are in the folder `PROT`, use these command:
`cp -r ../radial_distribution/ ./`
`cd ./radial_distribution/`


In the first step you will use the bash script `extract_coordinates.sh`. You have to grant execution permission to the script.
`chmod +x extract_coordinates.sh`


The bash script `extract_coordinates.sh` extracts the coordinates along the simulations of the particles comprising the centroids. The script performs three steps that are described. 

- First, the script creates an index file with the number of the BB particles necessary for computing the centroids of every peptide.
- Second, using this index and the GROMACS tool `trjconv`, the script creates a trajectory in `.gro` format for these atoms.
- Finally, the script creates three different files with the values of the coordinates, box dimension and timestamp from the trajectory file.


Edit the script `extract_coordinates.sh`. In this example you only have to edit the paths to directories and files. If you are using a peptide different from the example, you will also need to modify the following variables.

- The variable `FIRST_PEPTIDE_RESIDUE_UNIPROT` is the residue Uniprot numeration of the first residue in the input PDB file. In our case this number is 156 (serine) in accordance with the Uniprot sequence O14836.
- The variable `CENTER_RESIDUE_UNIPROT` is the Uniprot number of the residue selected for the centroids calculation. In this example we used 174 (valine).

>**Note:** if you are working with the example files, follow the instructions in the script to complete the edition. 

Once edited, grant execution permission and run the script.

```
chmod +x extract_coordinates.sh
./extract_coordinates.sh

```


The radial distribution is calculated with the Rscript `radial_distribution.R`. Open this file and edit the variables. You can also follow the comments in the code to understand the calculations. 

You can run this Rscript in RStudio or in a terminal with the next command.


```
Rscript ./radial_distribution.R 

```

The script creates a table with the columns: 

-   *Time*: time of the simulation
-   (*i,j*): number of peptides i and j interacting at distance less than cutoff
-   (*peptide_i,peptide_j*): peptide identification in letters code
-   *distance*: geometric distance between centroids
-   (*vdx,vdy*): (x,y) location of peptide j with respect to peptide i
-   *alpha*: alpha angle in degrees (-180 to 180) of the location of peptide j with respect to peptide i.
-   *beta*: angle between orientation vectors of peptide j with respect to peptide i.

In addition, the script creates two graphs in `.png` format. As shown in figure 7, these graphs are:

- `Radial_map_residueXXX.png`:  a density map of vdx vs. vdy for residue XXX accumulated along every interacting pair of peptides (i,j). The scatter plot is shown in the background.
- `Angular_map_residueXXX.png`: a density map of alpha angle vs. beta angle for residue XXX  accumulated along every interacting pair of peptides (i,j). The scatter plot is shown in the background.

Using the files we provide as example, you will obtain the next maps.


 ![](Figs/Radial_map_residue174.png)
 ![](Figs/Angular_map_residue174.png)

