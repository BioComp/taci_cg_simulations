Here, you will find instructions to run a production simulation with an array of 9 transmembrane peptides embedded in a lipid bilayer. From the all-atom structure of the transmembrane segment of human TACI ([Uniprot O14836](https://www.uniprot.org/uniprot/O14836)), you will create a coarse-grained model, prepare an array of 9 peptides and embed this array in a lipid bilayer composed of DOPC and DLPC (7:3). Finally, the system will include water particles and ions. This system will be used in the following steps for the molecular dynamic simulation.

# Table of content <!-- omit in toc -->

[[_TOC_]]


# Requirements
- Python 2.7
- [martinize.py](http://md.chem.rug.nl/index.php/tools2/proteins-and-bilayers/204-martinize)
- [insane.py](http://www.cgmartini.nl/index.php/downloads/tools/239-insane)
- [GROMACS](www.gromacs.org), version 2016 or superior
- [DSSP](https://swift.cmbi.umcn.nl/gv/dssp/),  version > 2.0.4
- Recommended: Pymol, [VMD](https://www.ks.uiuc.edu/Research/vmd/) or Chimera for molecular visualization 


# Membrane topology

Before beginning, clone or download this repository to your computer. Move to the repository folder and and create a working directory named as you like (*PROT* in this example). 


From the folder `example_files` copy the file `peptide.pdb` into this new folder. Also copy the whole folder `martiniff`. 

Then move to the folder `PROT`


```
mkdir  PROT
cp example_files/peptide.pdb PROT/
cp -r martiniff/ PROT/
cd PROT

```

`martiniff` contains the version of the Martini force field and the `lipids.itp` file used in our original works. You can also download these files from the Martini website. In our example, `peptide.pdb`corresponds to the helical structure of the human TACI transmembrane domain (residues 156 to 191) structure from AlphaFold (AF-O14836-F1).

Download a copy of the files [martinize.py](http://cgmartini.nl/index.php/tools2/proteins-and-bilayers/204-martinize) and [insane.py](http://www.cgmartini.nl/index.php/downloads/tools/239-insane) inside this folder and grant them execution permission with the next commands:

```
chmod +x martinize.py
chmod +x insane.py
```

> **Note:** The next instructions assume you are using `martinize.py` version 2.6 and the current version of `insane.py`, the same versions we used in our work. Using other versions, especially versions 3+, may have some changes in the syntax and requirements. Check the manuals in the Martini force field webpage. 

Be sure that [DSSP](https://swift.cmbi.umcn.nl/gv/dssp/) is installed. Copy the path of the executable from the output of the next command:

```
which dssp
which mkdssp
```

Usually the path is `/usr/bin/dssp` and `/usr/bin/mkdssp`. Try these paths with the option `-dssp` in the command in the next step.  

Then, convert an all-atom model of the peptide into a coarse-grain MARTINI model using `martiniza.py`. As a result, you will obtain a topology file named `peptide-CG.top` and a structure file named `peptide-CG.pdb`.


```
martinize.py  -f peptide.pdb \
              -o peptide-CG.top \
              -x peptide-CG.pdb \
              -ff elnedyn22 \
              -name Protein \
              -p None \
              -elastic -cys auto -eu 1.0 \
              -dssp /usr/bin/dssp \
              -nt \
              -sep 
```

>**Note:** if you have problems with dssp and martinize.py, try downloading the binary file we provide in our repository [dssp-2.0.4-linux-i386](./dssp/dssp-2.0.4-linux-i386), and change the path after the `-dssp` flag in the previous command to that file. A workaround could be  running the command in a Google Colab. Check this [Google Colab](https://colab.research.google.com/drive/1Mewofw8z4Duai2MAJ_kc1LV_BsaGCbmZ#scrollTo=fgjxMO4_j-z-)

>Check the `martinize.py --help` for more help. Here, with the option `-ff` you can use any of the force fields  available in the `/martinize.py` script: martini21, martini21p, martini22,  martini22p, elnedyn, elnedyn22  and elnedyn22p. In this example we decided to use elnedyn22. You can perform this step also with the new version of martinize3 which is a Python package.

>**Optional:** If you are curious, open the `peptide.pdb` and `peptide-CG.pdb` files with Pymol to visualize the input structure and the generated coarse grained model. To do this, first open pymol  and then type the following lines in the pymol console. You must replace `PATH` with the path where `peptide.pdb` and `peptide-CG.pdb` are located in your computer.  Consider the next command in Pymol to load and show the peptides as spheres.

```
load PATH/peptide.pdb
load PATH/peptide-CG.pdb
show spheres
bg_color  white

```

Next, using the coarse-grain peptide structure `peptide-CG.pdb` build a box where an array of 36 peptides are inserted in a membrane. With the next steps we create an array where the peptides are located equidistantly and randomly oriented around their z-axis. 

First, you have to build a box where a single helix is aligned to the z-axis. There are many ways to do this. We found that the next simple two steps using tools from GROMACS are very robust. 

Align the peptide to the principal axis of its containing box using the next command. Choose `System` (option 0)  as the group for determining the orientation.

`gmx editconf -f peptide-CG.pdb -c -princ -o aligned_peptide.gro`

Rotate the peptide 90 degrees around y-axis (or x-axis, as necessary). 

`gmx editconf -f aligned_peptide.gro -c -rotate 0 90 0 -o z_aligned_peptide.gro`

> **Note:** The location of the N-end of the peptide (towards the upper or the lower membrane layer) can be controlled rotating plus or minus 90 degrees. You can visualize the orientation of the peptide in VMD by using the reference XYZ axes on the bottom left corner of the model display window as follows:

> `vmd -m z_aligned_peptide.gro`


> In the menu `Graphics/Representations` you can create a representation of the BB atoms (type `name BB` in the Selected Atoms box) and choose `VDW` as the Drawing Methods to observe the BB particles as van der Waals spheres.    

Then, we use the next GROMACS command to create an array of 3 $\times$ 3 peptides. The distance between peptides in the xy-plane is control by the option `-dist 4 4 0`. The options `-rot -maxrot 0 0 180` ensure that every peptide is rotated randomly around the z-axis $\pm 180$ degrees. 

`gmx genconf -f z_aligned_peptide -nbox 3 3 1 -dist 4 4 0  -o peptide_array.gro  -rot -maxrot 0 0 180`

> **Note:** In the previous steps you have created several files with the `.gro` extension. Although some of them are temporary files or intermediate steps, do not erase them. They can be used in the analysis steps.

>*Optional:* As we did before, if you visualize the `peptide_array.gro` file with pymol, the structure will be like the one shown in figure 2.


Then, we use the `insane.py` script to create a membrane where this array will be embedded. Options `-x -y -z` control the dimensions (nm) of box. These values vary with the number of helices and distance between them as specified in the previous step. The compositions of the lower and upper layers of the membrane are selected with the options `-l` and `-u`. For alternative membrane compositions, check that the topologies of the lipids you select are included in the `lipids.itp` file.

```
insane.py -f peptide_array.gro \
          -o peptide_array_mem.gro \
          -p topol-insane.top \
          -salt 0.15 \
          -ring \
          -x 12 -y 12 -z 10.5 \
          -sol W -rand 0.095 \
          -l DLPC:3 -l DOPC:7 \
          -u DLPC:3 -u DOPC:7   
```

> **Note:** To calculate the  XY dimensions of the periodic box, consider that the distance from the peptides in the borders of the array to the nearest peptide in the periodic image has to be no less than the separation you specified in the previous command. The dimension in the Z axis is enough to create a layer of water beads that separate the membrane from its periodic image. You have to edit the XY values if you create a different array. For example, if you used  `-nbox 4 4 1 -dist 4 4 0` in the previous command, here you may use `-x 16 -y 16 -z 10.5`.

Make sure that you alredy copied the `martiniff` folder in the working directory.

Edit the topology file `.top` generated by `insane.py` since this file may not be consistent with the environment variables and the next steps could be aborted by errors. Open `topol-insane.top` with a plain text editor and edit as follows.

Open the file in gedit
`gedit topol-insane.top`

-   Remove the `#include` lines (usually the first one or two lines).
-   Copy the following lines at the beginning of the file (also copy the numerals. They are not comment symbols.  In the GROMACS syntax the comment symbol is `;`).


```
#include "martiniff/martini_v2.1.itp"
#include "martiniff/lipids.itp"
#include "martiniff/martini_v2.0_ions.itp"
#include "Protein_A.itp"
#ifdef POSRES 
#include "posre.itp" 
#endif 
```


Also, in this file you have to edit two variables:

* The internal name of the peptide. This name is defined under the section `[moleculetype ]` in  `Protein_A.itp`.  Copy that name.
* The number of replicas of the peptide. In this example we used 9 peptides in the system.

Under the section `[ molecules ]` in  `topol-insane.top` you will find a first uncommented line with the name and number of proteins. Be sure that the protein name corresponds to the name in `Protein_A.itp` and modify the number of peptides to 9. In our example, the correc line would be:

`Protein_A         9`

Next, generate a position restriction file necessary for the preparation steps before the simulation. Follow the next two steps.

**Note:** After the previous steps, the initial configuration of the system is composed as shown in the next figure, panel "a" and "b. The script [vmd_representation.tcl](./vmd_representation_scripts/vmd_representation.tcl) that produces these representations can be found in the repository. If you have `vmd` installed, copy the scripts in the folder [vmd_representation_scripts](./vmd_representation_scripts) (`cg_bonds-v5.tcl` and `vmd_representation.tcl`) in the current folder (`PROT` in this example) and run the following command:

`vmd -e vmd_representation.tcl`


If you visualize the `peptide_array_mem.gro` file with pymol, you will see a structure like the one shown in the panel "c".

!["Figure"](./Figs/Figure3.jpg)

Generate an index file with the atom number of the backbone (BB) particles. GROMACS has an interactive tool to do that `make_ndx`. In the help menu, you can check the examples to learn the syntax and choose your groups of atoms.  In this example the group is `BB`.  In the next box you have an interacting version of the command, where the options are already defined between the flags `<<OPT` and `OPT`. You only have to copy the four lines and press ENTER to run the script.  But if you only type the command (i.e, first line  except   `<<OPT`) the script will ask you to type your options. Then you have to type the lines 2 and 3 only. You can use this option to have a more GROMACS-like experience.

 
```
gmx make_ndx -f z_aligned_peptide.gro -o index_BB.ndx  <<OPT
aBB
q
OPT
```

Now, generate a position restriction file that will be useful for the preparation steps before the simulation. Run the command `genrestr` to generate a position restriction file using the index file generated in the previous step. The command asks you to choose an option, select the option corresponding to the group you created (check the output of `make_ndx`).

```
gmx genrestr -f peptide_array_mem.gro -n index_BB.ndx
```


# Molecular Dynamics Simulation

Now you will use the previously generated peptide array in a lipid bilayer for the molecular dynamic simulation. To do so, it is necessary to perform an initial minimization of the system, followed by a position restrained equilibration step and a free equilibration step. Finally, the production step will generate the simulation of the array for 1 microsecond.

## Minimization

Then, we have to perform an initial energy minimization of the system. In our workflow scheme we prefer to create nested folders for every simulation. So, in the working directory we create a directory for the minimization and then we move there.

```
mkdir minimization
cd minimization

```

GROMACS requires to create a `.tpr` as run input file. This is done with the `grompp` preprocessor tool that reads the topology and the coordinates, and writes the atomic description of the system. 

Copy the `minimization.mdp` file from the `mpd` folder into this directory and run the next command.


```
cp ../../mdps/minimization.mdp ./

gmx grompp -f ./minimization.mdp \
          -c ../peptide_array_mem.gro  \
          -r ../peptide_array_mem.gro \
          -p ../topol-insane.top 
```

Most probably, `grompp` will halt without creating the output file because of a warning message. Check the importance of the warning. Solve the problem if necessary or surpass this massage adding the option `-maxwarn 1` to grompp.

Once created the topol.tpr file, we are able to run the minimization with the following command.

`gmx mdrun -s topol.tpr`


This steps can take some minutes to finish.

## Equilibrations

First, we perform a position restrained equilibration step (POSRE) using the output configuration from the minimization.

Create a new directory and then move there.

```
mkdir POSRE
cd POSRE

```

Copy the `equilibration_POSRE.mdp` file into this directory and repeat the previous two commands for this equilibration.

```
gmx grompp -f ./equilibration_POSRE.mdp \
          -c ../confout.gro  \
          -r ../confout.gro \
          -p ../../topol-insane.top -maxwarn 4
gmx mdrun -s topol.tpr
```

Then, a free equilibration is performed. Create the corresponding directory, move there, copy the `equil.mdp` file into this directory, and run the `grompp` and `mdrun` commands. 


```
mkdir equil
cd equil
gmx grompp -f ./equilibration_FREE.mdp \
          -c ../confout.gro  \
          -r ../confout.gro \
          -p ../../../topol-insane.top -maxwarn 2
gmx mdrun -s topol.tpr
```

## Production

Before running the production simulation it is recommended to check that the system is properly equilibrated. We refer you to the [GROMACS user guide](https://manual.GROMACS.org/current/user-guide/index.html) for good practices and tips. 

If equilibration steps were successful, we are ready to initiate the production simulation. The procedure is similar to the previous ones. Create a new directory, move there, copy the `production_310K.mdp` file into this directory, and run the grompp and mdrun commands. 

```
mkdir production
cd production
gmx grompp -f ./production_310K.mdp \
          -c ../confout.gro  \
          -r ../confout.gro \
          -p ../../../../topol-insane.top 
gmx mdrun -s topol.tpr
```

> **Note:** In this example, you will only run 3 microseconds of simulation to speed up the process. In our published work we found that no less than 8 microseconds with a system of 36 helices is necessary to obtain significant results [(1,](https://doi.org/10.3389/fcell.2020.577278) [2)](https://doi.org/10.1016/j.celrep.2022.110583).

>Most probably you will need adding other options to `mdrun` to parallelize the simulation. These options strongly depend on the structure of your hardware. We recommend checking the [GROMACS user guide](https://manual.GROMACS.org/current/user-guide/index.html).
. 

>**Optional:** You can easily visualize the simulations. GROMACS provides a tool for viewing trajectories. It seems primitive, but give it a try:
`gmx view -f traj_comp.xtc -s topol.tpr`

>We prefer VMD. For example, you can load in VMD the simulation with the next command (use the `confout.gro` file from the equilibration so you do not need to wait for the simulation to finish):
>
`vmd -f ../confout.gro traj_comp.xtc` 

>In the menu Graphics/Representations you can create a representations of the BB atoms (type `name BB` in the selected atoms box) and chose VDW as the Drawing Methods to observe the BB particles as van der Walls spheres and the rest of the system as points. One initial step may look  like the next figure.

>!["Figure"](./Figs/simulation_box.png)

[Next: Contact analysis ](./2_contact_matrix.md)