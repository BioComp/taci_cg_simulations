##########################################
#
# Copy this file and the file 'cg_bonds-v5.tcl' into the PROT/ folder
# The PROT folder should contain the following files and folders:
# - cg_bonds-v5.tcl (a modified version )
# - peptide_array_mem.gro
# - topol-insane.top
# - martiniff/ folder and its contect
# 
# Run this script in a commnand line with vmd as follows:
# 
# vmd -e vmd_representation.tcl
# 
##########################################


mol addfile peptide_array_mem.gro

source ./cg_bonds-v5.tcl
cg_bonds -topoltype "elastic" -top topol-insane.top -cutoff 6

color Display Background white
display projection   Orthographic
display cuemode    Linear


mol selection "name BB"
mol addrep 0
set repid [expr [molinfo 0 get numreps] - 1] 
mol modstyle  $repid 0 Licorice 1.2 17 17
mol modcolor $repid 0  ColorID 3

mol selection "resname W"
mol addrep 0
set repid [expr [molinfo 0 get numreps] - 1] 
mol modstyle  $repid 0 VDW 1.2
mol modcolor $repid 0  ColorID 23

mol selection "resname \"D.PC\""
mol addrep 0
set repid [expr [molinfo 0 get numreps] - 1] 
mol modstyle  $repid 0 Licorice 0.3

