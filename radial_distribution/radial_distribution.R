rm(list=ls(all=TRUE))

############ START EDIT HERE IF YOU NEED ########################

# The directory where the scripts are downloaded
DIR_ANALYSIS <- "~/PATH/radial_distribution"
setwd(DIR_ANALYSIS)

N_helices   <- 9
peptide_direction	<- 1

# This number is varible CENTER_RESIDUE_UNIPROT in extract_coordinate.sh
central_residue <- 174

# Filter distance cutoff. Do not confuse with the cutoff of the contact matrix.
# This variable affects the pairs of peptiedes reported in the output. 
# If you reduce the number, the output file will be smaller
# This value affects the outter limit of data that you observe in the
# final vdx-vdy scatter plot.
cutoff  <-  3

############ END EDIT HERE ############################################




# FUNCTIONS ###################################
# Geometry (Peter Kahn)

Vectors<-function(x1,x2,x3,box){
  # Orientation vectors for every peptide at the centroid residue (central_residue).
  
  # Uncorrected distance (después se corrige)
  boxx<-matrix(rep(box,dim(x1)[2]),ncol=dim(x1)[2])
  Dx2<-x1-x2
  
  # Periodic boundaries correction
  x2[(Dx2 >=  boxx/2)]  <- x2[(Dx2 >=  boxx/2)] + boxx[(Dx2 >=  boxx/2)]
  x2[(Dx2 <= -boxx/2)]  <- x2[(Dx2 <= -boxx/2)] - boxx[(Dx2 <= -boxx/2)]
  Dx3<-x1-x3
  x3[(Dx3 >=  boxx/2)]  <- x3[(Dx3 >=  boxx/2)] + boxx[(Dx3 >=  boxx/2)]
  x3[(Dx3 <= -boxx/2)]  <- x3[(Dx3 <= -boxx/2)] - boxx[(Dx3 <= -boxx/2)]
  
  # Mass center
  comx<-(x1+x2+x3)/3
  
  #  Periodic boundaries correction of center of mass
  comx[(comx < 0)] <- comx[(comx < 0)] +  boxx[(comx < 0)]
  comx[(comx > boxx)] <- comx[(comx > boxx)] -  boxx[(comx > boxx)]
  
  # Bisection vector. Orientation of the peptide with respect to box XY-axes
  SumVectors<-x3+x1-2*x2
  
  return(list("V"=SumVectors, "COM"=comx))
}

Distcom<-function(comx,comy,comz,Dx, Dy, Dz, box,serie){
  # Matrix of distance between centroids center_of_mass
  # Alpha angles on the XY plane are also calculated
  # comx,comy, comz: xyz coordinates of centroid center of mass 
  # Dx, Dy, Dz: xyz  components of bisection vector per centroid
  # box: box dimensions at time 't'
  # series: list of peptides to analyse


  distance<-c()
  alpha<-c()
  namecols<-c()
  index_i <- c()
  index_j <- c()
  vdx<-c()
  vdy<-c()
  vdz<-c()
  distance.x <- c()
  distance.y <- c()
  
  #  Iterations 
  indices<-c(1:(length(serie)))
  for(n in indices){
    i<-serie[n]
    for(m in indices[-n]){
      j<-serie[m]
      dx<-(comx[,j]-comx[,i])
      dx[(dx >=  box[,1]/2)]  <- dx[(dx >=  box[,1]/2)] - box[(dx >=  box[,1]/2),1]
      dx[(dx <= -box[,1]/2)]  <- dx[(dx <= -box[,1]/2)] + box[(dx <= -box[,1]/2),1]
      
      dy<-(comy[,j]-comy[,i])
      dy[(dy >=  box[,2]/2)]  <- dy[(dy >=  box[,2]/2)] - box[(dy >=  box[,2]/2),2]
      dy[(dy <= -box[,2]/2)]  <- dy[(dy <= -box[,2]/2)] + box[(dy <= -box[,2]/2),2]
      
      dz<-(comz[,j]-comz[,i])
      dz[(dz >=  box[,3]/2)]  <- dz[(dz >=  box[,3]/2)] - box[(dz >=  box[,3]/2),3]
      dz[(dz <= -box[,3]/2)]  <- dz[(dz <= -box[,3]/2)] + box[(dz <= -box[,3]/2),3]
      #
      
      distance  <- cbind(distance,sqrt((dx^2) + (dy^2) + (dz^2) ))
      index_i <- c(index_i,i)
      index_j <- c(index_j,j)
      namecols<-c(namecols,paste(i,j,sep="_"))
     
      # Distance Vectors are projected over the XY components of the orientation Vectors
      vdx<-cbind(vdx, dx*Dx[,i]+dy*Dy[,i])
      vdy<-cbind(vdy,-dx*Dy[,i]+dy*Dx[,i])
      
  
      # vdz<-cbind(vdz,dz)
    }
  }
  alpha<-cbind(alpha,atan2(vdy,vdx)*180/pi)
  colnames(alpha)<-namecols
  colnames(distance)<-namecols
  colnames(vdx)<-namecols
  colnames(vdy)<-namecols
  index_pairs <- cbind(index_i,index_j)
  return(list("distance"=distance, "alpha"=alpha, "vdx"=vdx, "vdy"=vdy, "index_pairs"=index_pairs))  
}

BetaAngles<-function(Dx,Dy,serie){
  orientation<-atan2(Dy,Dx)
  beta<-c()
  indices<-c(1:(length(serie)))

  # Iteration
  for(n in indices){
    i <- serie[n]
    for(m in indices[-n]){
      j <- serie[m]
      beta<-cbind(beta,(orientation[,j] - orientation[,i])*180/pi )
    }
  }

  beta[(beta < -180)] <- beta[(beta < -180)] + 360
  beta[(beta >  180)] <- beta[(beta >  180)] - 360
  
  return(list("beta"=beta))
}


##### Read files with data  ############
Archivo1 <- paste0("./coord_residue_",central_residue,".dat")
data1  <-  as.matrix(read.table(Archivo1,header=F,sep=""))

Archivo2 <- paste0("./box_residue_",central_residue,".dat")
box  <-  as.matrix(read.table(Archivo2,header=F,sep=""))

Archivo3 <- paste0("./times_residue_",central_residue, ".dat")
times  <-  as.vector(read.table(Archivo3,header=F,sep=""))

BBatoms <- N_helices*3

##### Reformat data

# Create three matrices with coordinates x, y, z.
# Each column is a BB particle. 

x <- matrix(t(data1[,1]),ncol=BBatoms,byrow = T)
y <- matrix(t(data1[,2]),ncol=BBatoms,byrow = T) * peptide_direction 
z <- matrix(t(data1[,3]),ncol=BBatoms,byrow = T) * peptide_direction 

# Take coordinates xyz to particles 1, 2 or 3 of every peptide/centroid
x1 <- x[,c(T,F,F)]
x2 <- x[,c(F,T,F)]
x3 <- x[,c(F,F,T)]

y1 <- y[,c(T,F,F)]
y2 <- y[,c(F,T,F)]
y3 <- y[,c(F,F,T)]

z1 <- z[,c(T,F,F)]
z2 <- z[,c(F,T,F)]
z3 <- z[,c(F,F,T)]


## Center of mass and Orientation vectors per peptide/centroid #########

Mx <- Vectors(x1,x2,x3,box[,1])
My <- Vectors(y1,y2,y3,box[,2])
Mz <- Vectors(z1,z2,z3,box[,3])


Bisec.x <- Mx$V # bisection vectors (component x)
comx <- Mx$COM  # centers of mass (component x)
Bisec.y <- My$V # bisection vectors (component  y)
comy <- My$COM  # centers of mass (component y)
Bisec.z <- Mz$V # bisection vectors (component  z)
comz <- Mz$COM  # centers of mass (component z)

# Unitary bisection vector
modulo <- sqrt(Bisec.x^2 + Bisec.y^2 + Bisec.z^2)
BisecN.x <- Bisec.x/modulo
BisecN.y <- Bisec.y/modulo
BisecN.z <- Bisec.z/modulo

# Unitary bisection vector (xy componentes only)
modulo.xy <- sqrt(Bisec.x^2 + Bisec.y^2)
BisecN.x.2d <- Bisec.x/modulo.xy
BisecN.y.2d <- Bisec.y/modulo.xy


######################################
# Peptide series
# Peptide/centroids are numbered 1...N
serie <- c(1:N_helices)

# Distances between center of mass of centroids
Distances <- Distcom(comx, comy,comz,BisecN.x.2d,BisecN.y.2d,BisecN.z,box,serie)

# Calcultaion of distance, alpha, beta and xy locations
distance1 <- Distances$distance # modulo of XYZ distance
alpha <- Distances$alpha              # Angle on XY plane
vdx  <- Distances$vdx                 # Distance component X
vdy  <- Distances$vdy                 # Distance component Y
index_pairs <- Distances$index_pairs # peptide pairs (i , j) along time (rows)
beta <- BetaAngles(BisecN.x.2d,BisecN.y.2d,serie)$beta # angles on XY plane


######################################
# Mutual radial location

# Filter by distance cutoff
# cutoff  is difined in the first section


# table of times and pairs in contact (dist < cutoff)
snaps <- which(distance1 < cutoff,arr.ind = T)
colnames(snaps) <- c("times","i_j")

# Filter all matrices by distance
distance_sub  <-  distance1[snaps]
vdx_sub <- vdx[snaps]
vdy_sub <- vdy[snaps]
alpha_sub <- alpha[snaps]
beta_sub <- beta[snaps]
times_sub <- times[snaps[,"times"],]
indices_sub <- index_pairs[snaps[,"i_j"],]

		
######## Save data ##########

chains <- c(LETTERS,letters)

output_dt <- data.frame("Time"=times_sub,
		                 "a" = indices_sub[,1],
		                 "b" = indices_sub[,2],
		                 "peptide_a"=chains[indices_sub[,1]],
		                 "peptide_b"=chains[indices_sub[,2]],
		                 "distance"=distance_sub,
		                 "vdx"=vdx_sub,
		                 "vdy"=vdy_sub,
		                 "alpha"=alpha_sub,
		                 "beta"=beta_sub)

# Format RDS
saveRDS(output_dt, file=paste0("./xyR_residue",central_residue,".RDS"),compress = "bzip2")

# Format csv
write.csv(output_dt,  file=paste0("./xyR_residue",central_residue,".csv"), quote = FALSE, row.names = FALSE)



# Plots ################
require(ggplot2)
  
  
Plot <- function(dt,x,y,xlim,ylim,xlab,ylab,Title=""){
  ggplot(dt, aes_string(x=x, y=y)) +
    geom_point(colour= "Grey60", size=0.3, shape=20, alpha=0.2) +
    stat_density_2d(aes(fill = stat(level), color = stat(level)),
                    contour_var = "ndensity",  
                    geom = "polygon", 
                    contour = T,
                    size = 0.4) +
    scale_fill_distiller(palette = "Greys", direction=1, name=NULL) +
    scale_colour_distiller(palette= "Greys", direction=-1, name=NULL) +
    xlim(xlim) +
    ylim(ylim) +
    labs(x = xlab, y = ylab, fill="Density") +
    theme_grey(base_size = 14) +
    guides(color = "none" ) +
    theme(legend.position = "top", legend.text = element_text(size = 11),
          legend.key.width = unit(1.5,"cm"), legend.background=element_rect(size=0.5, linetype="solid", colour="black"),
          aspect.ratio = 1, plot.title = element_text(hjust = 0.5), 
          axis.text.x = element_text(size = 12, angle = 0, vjust = 0.3, face = "bold"),
          axis.text.y = element_text(size = 12, angle = 0, vjust = 0.3, face = "bold"),
          panel.background = element_rect(fill = "white", colour = "black", size = 2, linetype = "solid"),
          panel.grid.major = element_line(size = 1, linetype = 'solid', colour = "grey80"), 
          panel.grid.minor = element_line(size = 0.5, linetype = 'solid', colour = "grey80"))+
    ggtitle(Title)
}

Plot(output_dt,
    "vdx",
    "vdy",
    xlim=c(-3,3),
    ylim=c(-3,3),
     "x (nm)",
     "y (nm)",
    Title = "Radial distribution"
    )+
  geom_point(aes(x=0, y=0), colour="Black", size=5, shape=10, stroke=1) +
  geom_segment(aes(x = -0.3, y = 0, xend = 0.3, yend = 0), colour = "Black",
               lineend = 'round', linejoin = 'mitre', size = 0.6, 
               arrow = arrow(length = unit(0.05, "inches")))
  
    

# Save the plot
ggsave(paste0("Radial_map_residue",central_residue,".png"),
      path = "./" , width = 5.5, height = 4 ,scale = 1.4)


# Angular map

# To reproduce the figures presented in the paper, angle values are 
# recalculated in a range of 0-360°. So, values<0 are added 360 degrees 
# Remenber that the data save previously do not have this transformation.

output_dt[output_dt$alpha < 0,"alpha"] <- output_dt[output_dt$alpha < 0,"alpha"] + 360
output_dt[output_dt$beta  < 0,"beta" ] <- output_dt[output_dt$beta  < 0,"beta" ] + 360

# Plot
Plot(output_dt,
    "alpha",
    "beta",
    xlim=c(0,360),
    ylim=c(0,360),
   "Alpha (degree)",
   "Beta (degree)",
    Title = "Angular distribution"
    )

# Save the plot
ggsave(paste0("Angular_map_residue",central_residue,".png"),
      path = "./" , width = 5.5, height = 4 ,scale = 1.4)

