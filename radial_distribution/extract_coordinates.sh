#!/bin/bash

############ START EDIT HERE IF YOU NEED ########################


# Folder where trajectories are saved
TRJDIR="../minimization/POSRE/equil/production/"

# EXAMPLE! If you are using the files in example_file, 
# comment the previous line and uncomment the next one
# TRJDIR="PATH/example_files/"


# File from where to extract the BB particle numbers
FILE="../minimization/confout.gro"

# EXAMPLE! If you are using the files in example_file, 
# comment the previous line and uncomment the next one
# FILE="${TRJDIR}/conf_for_topol.gro"

# If you are not interested in Unipror numbers use values 1 and N 
# for the next two variables. N is the number of residue in your
# peptide and 1 is the first residue.

# Uniprot number of the first residue of your peptide
FIRST_PEPTIDE_RESIDUE_UNIPROT=156

# Uniprot number of the central residue for centroid calculations
CENTER_RESIDUE_UNIPROT=174

# Number of residues per peptide
Nres=36

# Number of peptides in tue simulation
N_helices=9

############ END EDIT HERE #################################










#################################################
############### PROCEDURE #######################
#################################################

############### 1. INDEX CREATION #######################

# Previous and posterior residues;
CRES=$((${CENTER_RESIDUE_UNIPROT} - ${FIRST_PEPTIDE_RESIDUE_UNIPROT} + 1))
ires=$((${CRES} - 1))
jres=$((${CRES} + 1))

# Let's make the index with the 3-residues from each peptide
echo -n > ./index_residue_${CENTER_RESIDUE_UNIPROT}.ndx
echo "[ selected_particles ]" >> ./index_residue_${CENTER_RESIDUE_UNIPROT}.ndx


for X in $(seq 0 $(( ${N_helices} - 1 )))
do
	ini=$(($ires + $X * $Nres))
	fin=$(($jres + $X * $Nres))
	awk -v a="$ini"  -v b="$fin"  ' $1+0 >= a && $1+0 <= b && $2 ~ /BB/ {print $3}' ${FILE} >> ./index_residue_${CENTER_RESIDUE_UNIPROT}.ndx
done

###############  2. BB particles TRAJECTORY #######################

# Create a trajectory in .gro format
echo selected_particles | gmx trjconv -f ${TRJDIR}/traj_comp.xtc \
		-s ${TRJDIR}/topol.tpr \
		-n ./index_residue_${CENTER_RESIDUE_UNIPROT}.ndx \
		-o ./traj-BB_residue_${CENTER_RESIDUE_UNIPROT}.gro  \
		-dt 1


######### EXAMPLE! ################################################################

#  If you are using the files in the example_files folder, comment the last command
# and uncomment the next five lines . 
# Check the paths to the files and folder

# echo selected_particles | gmx trjconv -f ${TRJDIR}/traj_protein.xtc \
# 		-s ${TRJDIR}/conf_for_topol.gro \
# 		-n ./index_residue_${CENTER_RESIDUE_UNIPROT}.ndx \
# 		-o ./traj-BB_residue_${CENTER_RESIDUE_UNIPROT}.gro  \
# 		-dt 1


# Extact information from trajectory
awk ' $1 ~ /[0-9]+[A-Z]{3}/ {print $4" "$5" "$6" "}' ./traj-BB_residue_${CENTER_RESIDUE_UNIPROT}.gro > ./coord_residue_${CENTER_RESIDUE_UNIPROT}.dat
awk ' NF==3 && $1 !~/[A-Z]/ && $2 !~/[A-Z]/ && $3 !~/[A-Z]/ {print} ' ./traj-BB_residue_${CENTER_RESIDUE_UNIPROT}.gro > ./box_residue_${CENTER_RESIDUE_UNIPROT}.dat
grep 't=' ./traj-BB_residue_${CENTER_RESIDUE_UNIPROT}.gro | sed -e 's/.*t=//g' | awk '{print $1}'  > ./times_residue_${CENTER_RESIDUE_UNIPROT}.dat

# trajectory in .gro format are no longer necessary. 
# You can delete this file uncommenting the next line
# rm ./traj-BB_residue_${CENTER_RESIDUE_UNIPROT}.gro

