

Coarse-grain molecular dynamics simulations of TACI transmembrane domain
===========

Here we describe a protocol to prepare and perform a molecular dynamics simulation of replicas of a peptide embedded in a lipid bilayer. We describe the preparation of a coarse-grained model using the Martini force field, as described in the papers 

- [Coarse Grained Molecular Dynamic Simulations for the Study of TNF Receptor Family Members' Transmembrane Organizationl](https://doi.org/10.3389/fcell.2020.577278)
- [Ligand-independent oligomerization of TACI is controlled by the transmembrane domain and regulates proliferation of activated B cells](https://doi.org/10.1016/j.celrep.2022.110583).


# Table of content

[[_TOC_]]

--------------
This protocols is described to be performed with a Linux OS (operative system). The computer also needs to have at least 8GB RAM and a processor similar or superior to Intell Corei5. If you intend to use this protocol to run a simulation to obtain results statistically  significant, consider using a cluster where you can use 10-14 cores per simulation.

In the next protocol you will prepare a system composed of 9 peptides embedded in a lipid bilayer. The system also includes particles of waters and ions. Consider that in our original publication we used a much larger system composed of 36 replicas of our peptides.


To perform this protocol you need the following software installed in your computer:

- [GROMACS](www.gromacs.org), version 2016 or superior
- R, version 3 or superior
- [DSSP](https://swift.cmbi.umcn.nl/gv/dssp/), version 2.0.4 or superior
- Python 2.7.
- [martinize.py](http://cgmartini.nl/index.php/tools2/proteins-and-bilayers/204-martinize): version 2,x. It is a Python script obtained from [Martini website](http://wwwartini.nl/).
- [insane.py](http://www.cgmartini.nl/index.php/downloads/tools/239-insane). It is a Python script obtained from  [Martini website](http://wwwartini.nl/).

 

We also recommend the installation of additional software that is not essential to perform this protocol

1. [RStudio](https://www.rstudio.com/), version 3 or superior. It is helpful to run and debug the R srcipts
2. [VMD](https://www.ks.uiuc.edu/Research/vmd/) or pymol for the visualization.

GROMACS, R, Python 2.7, DSSP and pymol are available in the Linux repositories of Debian and Ubuntu. To install them, open a terminal and type the next commands:

```
sudo apt install gromacs
sudo apt install r-base
sudo apt install python2.7
sudo apt install dssp
sudo apt install pymol
```


# 1. Simulations

[ Instruction for simulations ](./1_simulations.md)

___
# 2. Analysis
## 2.1. Contact matrix analysis
[ Instructions and scripts to calculate the contact matrix ](./2_contact_matrix.md)
## 2.2. Radial and angular distribution
[ Instructions and scripts to calculate the radial distribution ](./3_radial_distribution.md)

--------------