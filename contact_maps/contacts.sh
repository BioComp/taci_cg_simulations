#!/bin/bash


############ START EDITION OF VARIABLES HERE ########################

# cutoff distance (nm)
co=0.8

# Directory of production simulation
TRJDIR="../minimization/POSRE/equil/production/"
[[ ! -d ${TRJDIR}  ]] && echo "Directory ${TRJDIR} does not exist" && exit

### EXAMPLE! If you are using the files in example_file,  ###
### comment the previous line and uncomment the next one  ###
# TRJDIR="PATH/example_files/"

# Directory where indices are saved
NDXDIR="./indices"

# Directory to save output files
OUTPUTDIR="./ouput_contacts"
[[ ! -d ${OUTPUTDIR}  ]] && echo "Directory ${OUTPUTDIR} does not exist. Maybe you have to create it" && exit


############ END EDITION HERE #################################






############### PROCEDURE ################################

for i in $(cat ./indices/list_of_g1_atoms )
do

	NDX=${NDXDIR}/index${i}.ndx

	gmx select  -f ${TRJDIR}/traj_comp.xtc \
				-s ${TRJDIR}/topor.tpr \
				-rmpbc \
				-pbc \
				-select "(within ${co} of group \"group1\") and group \"group2\"" \
				-oi ${OUTPUTDIR}/select_${i}.dat -n ${NDX} \
				-dt 100

######### EXAMPLE! ################################################################

# If you are using the files in the example_files folder, comment the last command
# and uncomment the next five command lines. These files example are intended to be lightweight
# and reaady ot used in any (several) GROMACS version.
# This is an imperfect solution since you need a topol.tpr file to avoid break 
# molecules when they cross periodic boundaries. 
# Check the paths to the files and folder

	# gmx select  -f ${TRJDIR}/traj_protein.xtc \
	# 			-s ${TRJDIR}/conf_for_topol.gro \
	# 			-rmpbc \
	# 			-pbc \
	# 			-select "(within ${co} of group \"group1\") and group \"group2\"" \
	# 			-oi ${OUTPUTDIR}/select_${i}.dat -n ${NDX} \
	# 			-dt 100

done
